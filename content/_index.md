This browser add-on improves the readability of the U-Today website, whenever you visit it!

- Created with love by [Willem Mulder](https://git.snt.utwente.nl/14mRh4X0r)
- Converted with care by [Bob van de Vijver](https://git.snt.utwente.nl/bobv)

# Installation

1. Download the latest [build](https://git.snt.utwente.nl/utomorrow/browser-addon/-/jobs/artifacts/master/download?job=build), or select a specific [version](https://git.snt.utwente.nl/utomorrow/browser-addon/-/jobs)
2. Extract it somewhere you want
3. Follow the steps for your browser (see below)
4. It works!

**Chrome**<br>
1. Visit `chrome://extensions` (via omnibox or menu -> Tools -> Extensions)
2. Enable Developer mode by ticking the checkbox in the upper-right corner
3. Click on the "Load unpacked extension..." button
4. Select the directory containing your unpacked extension

**Edge**<br>
1. Visit `edge://extensions` (via omnibox or menu -> Extensions)
2. Enable Developer mode by ticking the checkbox in the lower-left corner
3. Click on the "Load unpacked extension..." button
4. Select the directory containing your unpacked extension

**Firefox**<br>
1. Visit `about:debugging#/runtime/this-firefox` (via address bar or menu -> add-ons -> settings -> debug add-ons)
2. Click the "Load temporary add-on" button
3. Select the directory containing your unpacked extension

**Other browsers?**
<br>If it supports Chromium extensions, it should work. Look for a way to load an unpacked extension.
